package com.example.springbook.model;

import org.hibernate.validator.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "books")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)

public class Buku implements Serializable{

@Id
@GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
private Long id;

@NotBlank
private String titleBook;

@NotBlank
private String namaDepanPengarang;

@NotBlank
private String namaBelakangPengarang;

@NotBlank
private int statusPeminjaman;

@NotBlank
private String namaPeminjam;

@Column(nullable = false, updatable = false)
@Temporal (TemporalType.TIMESTAMP)
@CreatedDate
private Date createdAt;

@Column(nullable = false)
@Temporal(TemporalType.TIMESTAMP)
@LastModifiedDate
private Date updatedAt;

public Long getId() {
return id;
}

public String getTitleBook() {
return titleBook;
}

public String getNamaDepanPengarang() {
return namaDepanPengarang;
}

public String getNamaBelakangPengarang() {
return namaBelakangPengarang;
}

public int getStatusPeminjaman() {
return statusPeminjaman;
}

public String getNamaPeminjam() {
return namaPeminjam;
}

public Date getCreatedAt() {
return createdAt;
}

public Date getUpdatedAt() {
return updatedAt;
}

public void setId(Long id) {
this.id = id;
}

public void setTitleBook(String titleBook) {
this.titleBook = titleBook;
}

public void setNamaDepanPengarang(String namaDepanPengarang) {
this.namaDepanPengarang = namaDepanPengarang;
}

public void setNamaBelakangPengarang(String namaBelakangPengarang) {
this.namaBelakangPengarang = namaBelakangPengarang;
}

public void setStatusPeminjaman(int statusPeminjaman) {
this.statusPeminjaman = statusPeminjaman;
}

public void setNamaPeminjam(String namaPeminjam) {
this.namaPeminjam = namaPeminjam;
}

public void setCreatedAt(Date createdAt) {
this.createdAt = createdAt;
}

public void setUpdatedAt(Date updatedAt) {
this.updatedAt = updatedAt;
}
}